import { Component, Input, OnInit } from '@angular/core';
import { RouletteService } from './shared/roulette.service';
import { NumberModel } from './shared/number.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  @Input() numbers!: NumberModel;
  number!: number;
  numbersArray: number[] = [];
  balance: number = 100;
  constructor(private rouletteService: RouletteService ) {}

  ngOnInit() {
    console.log(this.numbersArray)
    this.rouletteService.newNumber.subscribe((number: number) => {
      console.log(number)
      this.number = number;
      if(this.number) {
        this.numbersArray.push(this.number);
      }

    })
  }

  clickStart() {
    this.rouletteService.start()
  }

  clickStop() {
    this.rouletteService.stop()
  }

  clickReset() {
    this.numbersArray.splice(0, this.numbersArray.length);
  }
}
