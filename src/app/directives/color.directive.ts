import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';
import { RouletteService } from '../shared/roulette.service';

@Directive({
  selector: '[appColor]'
})

export class ColorDirective {
  @Input() set appColor(number: string) {
    if(number) {
      this.colorClass = this.rouletteService.getColor(this.number);
    }
  }
  number!: number;

  colorClass = 'red';
  constructor(private rouletteService: RouletteService, private el: ElementRef, private renderer: Renderer2) {}





}
