import { EventEmitter }  from '@angular/core';

export class RouletteService {
  newNumber = new EventEmitter();
  numberArray!: number[];
  interval: any;

  generateNumber() {
    return Math.floor(Math.random() * (36 + 1));
  }

  start() {
    this.interval = setInterval(() => {
      this.newNumber.emit(this.generateNumber());
    }, 1000);
  }

  stop() {
    clearInterval(this.interval);
  }


  getColor(number: number) {
    if(number === 0) {
      return 'zero';
    } else if (number >= 1 && number <= 10 || number >= 19 && number <= 28) {
      if (number % 2 === 0) {
        return 'black'
      } else {
        return 'red'
      }
    } else if (number >= 11 && number <= 18 || number >= 29 && number <= 36) {
      if (number % 2 === 0) {
        return 'red'
      } else {
        return 'black'
      }
    } else {
      return 'unknown'
    }
  }
}

